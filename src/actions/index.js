export const nextTopic=()=>({
    type:'NEXT_TOPIC',
});
export const backTopic=()=>({
    type:'BACK_TOPIC'
})

export const nextQuestionEvaluation=(value, typeQuestion)=>({
    type:'NEXT_QUESTION_EVALUATION',
    valueUser:value,
    typeQuestion:typeQuestion
})
