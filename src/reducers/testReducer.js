import question from "../question";

const initialState = {
    question: question(),
    actualQuestion: 1,
    valueByType: [
        {id: 1, type: 'INFORMACION BASICA', value: 0},
        {id: 2, type: 'CARGA Y RITMO DE TRABAJO', value: 0},
        {id: 3, type: 'DESARROLLO DE COMPETENCIAS', value: 0},
        {id: 4, type: 'LIDERAZGO', value: 0},
        {id: 5, type: 'MARGEN DE ACCIÓN Y CONTROL', value: 0},
        {id: 6, type: 'ORGANIZACIÓN DEL TRABAJO', value: 0},
        {id: 7, type: 'RECUPERACIÓN', value: 0},
        {id: 8, type: 'SOPORTE Y APOYO', value: 0},
        {id: 9, type: 'OTROS PUNTOS IMPORTANTES', value: 0},
    ]
}


export default function (state = initialState, action) {
    switch (action.type) {
        case 'NEXT_QUESTION_EVALUATION':
            console.log(state.question.length)
            return {
                ...state,
                actualQuestion: state.actualQuestion < state.question.length  ? state.actualQuestion + 1 : state.question.length,
                valueByType: action.typeQuestion
            }
        case 'BACK_TOPIC':
            return {
                ...state,
                actualTopic: state.actualTopic >= 1 ? state.actualTopic - 1 : 1
            }
        default:
            return state

    }

}

