import {combineReducers} from 'redux'
import learnReducer from "./learnReducer";
import testReducer from "./testReducer";

export  default combineReducers({
    learn: learnReducer,
    evaluation: testReducer
})
