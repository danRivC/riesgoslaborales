import content from "../content";
import question from "../question";

const initialState = {
    content: content(),
    actualTopic: 1
}


export default function (state = initialState, action) {
    switch (action.type) {
        case 'NEXT_TOPIC':
            return {
                ...state,
                actualTopic: state.actualTopic< state.content.length-1? state.actualTopic+1:state.content.length-1
            }
        case 'BACK_TOPIC':
            return {
                ...state,
                actualTopic: state.actualTopic>=1? state.actualTopic -1: 1
            }
        default:
            return state
    }
}

