import React from "react";
import cloud from "../../assets/images/cloud.svg";
import cloudAlone from "../../assets/images/C.svg";
import {backTopic, nextQuestionEvaluation, nextTopic} from "../../actions";
import {connect} from "react-redux";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {useState} from "react";
import {withStyles} from "@material-ui/core/styles";
import {blue} from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import {Link, Redirect} from "@reach/router";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const ColorButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(blue[500]),
        backgroundColor: blue[500],
        '&:hover': {
            backgroundColor: blue[700],
        },
    },
}))(Button);
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
const Test = ({evaluation, nextQuestionEvaluation}) => {
    const [question] = evaluation.question.filter((quest) => quest.id === evaluation.actualQuestion)
    const [valueSelected, useValueSelected] = useState(0)
    const [valueByType, useValueByType] = useState(evaluation.valueByType)
    const [aceptTerms, useAceptTerms] = useState(false)

    const handleChange = (event) => {
        useValueSelected(event.target.value)
    };
    const sendAnswer = () => {
        if (valueSelected !== 0) {
            updateValueByType().then(() => {
                nextQuestionEvaluation(valueSelected, valueByType)
                useValueSelected(0)
            })
        }

    }
    const updateValueByType = async () => {
        useValueByType(valueByType.map((tp) => {
            {
                if (tp.type === question.type) {
                    tp.value += parseInt(valueSelected)
                    return tp
                }
                return tp
            }
        }))
    }
    const showResult = () => {
        return (
            <Redirect to={'result'}/>
        )
    }
    const showOrHideDialog=()=>(
        useAceptTerms(!aceptTerms)
    )

    return (
        <div className={'bg-gray-200 grid grid-cols-1 md:pt-10 md:pl-0 h-full'}>
            <div className={'grid grid-cols-1 gap-64 z-0 absolute w-full'}>
                <div className={'hidden md:flex justify-end'}>
                    <img src={cloud} className={'ml-64'} alt=""/>
                </div>
            </div>
            <Dialog
                open={!aceptTerms}
                TransitionComponent={Transition}
                keepMounted

                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">{"Atencion estas a punto de iniciar el test"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        En este test se va a evaluar la calidad de seguridad ocupacional que tienes en tu trabajo.
                        Una vez iniciada la evaluación tienes que terminarla o no se guardaran tus respuestas.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={showOrHideDialog} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <div className={'grid grid-cols-1 z-10 '}>
                <div className={'transition duration-500 ease-in-out md:ml-20 md:pt-12 pl-10 pt-10 '}>

                </div>
                <div className={'md:mt-20 text-center'}>
                    <h2 className={'text-1xl md:text-4xl text-gray-500 text-bold'}>{question.tittle}</h2>
                </div>
                <div className={'mt-6 px-4 text-center'}>
                    <FormControl component="fieldset">
                        <span className={'text-xl text-gray-600 text-bold'}>Selecciona una Opción</span>
                        <RadioGroup name="options" value={valueSelected} onChange={handleChange}>
                            {question.options.map((quest) => (
                                <FormControlLabel className={'text-gray-700 text-bold '}
                                                  value={quest.value.toString()}
                                                  control={<Radio color="primary"/>}
                                                  label={quest.tittle}
                                                  key={quest.id}/>
                            ))}
                        </RadioGroup>
                    </FormControl>
                </div>

            </div>
            <div className={'flex flex-row justify-between md:content-end md:pl-0  md:pb-0  w-full flex-wrap'}>
                <div className={'hidden md:flex'}>
                    <img src={cloudAlone} alt="" className={'cloud-alone'}/>
                </div>


                <div className={'md:mr-64 md:ml-0 ml-48'}>
                    {

                        evaluation.question.length === evaluation.actualQuestion ?
                            <Link to={'results'}>
                                <ColorButton variant="contained" color="primary">
                                    <span className={'md:text-xl'}>
                                        Ver Resultados
                                    </span>

                                </ColorButton>
                            </Link>
                            :
                            <ColorButton disabled={valueSelected === 0} variant="contained" color="secondary"
                                         onClick={sendAnswer}>
                                <span className={'md:text-xl'}>Continuar</span>
                            </ColorButton>
                    }

                </div>
            </div>

        </div>
    )

}
const mapStateToProps = (state) => {

    return {
        evaluation: state.evaluation
    }
}
const mapDispatchToProps = {
    nextQuestionEvaluation
}
export default connect(mapStateToProps, mapDispatchToProps)(Test);
