import React, {useState, useEffect} from "react";
import cloud from "../../assets/images/cloud.svg";
import cloudAlone from "../../assets/images/C.svg";
import {nextQuestionEvaluation} from "../../actions";
import {connect} from "react-redux";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

const Result = ({evaluation}) => {
    const evaluationType = evaluation.valueByType
    const [total, useTotal] = useState(0)




    return (
        <div className={'bg-gray-200 grid grid-cols-1 md:pt-10 md:pl-0 h-full'}>
            <div className={'grid grid-cols-1 gap-64 z-0 absolute w-full'}>
                <div className={'hidden md:flex justify-end'}>
                    <img src={cloud} className={'ml-64'} alt=""/>
                </div>
            </div>
            <div className={'grid grid-cols-1 z-10 '}>
                <div className={'transition duration-500 ease-in-out md:ml-20 md:pt-12 pl-10 pt-10 '}>

                </div>
                <div className={'md:mt-6 text-center'}>
                    <h2 className={'text-4xl md:text-6xl text-gray-800 text-bold'}>Los resultado de la evaluacion de
                        calidad son los siguientes</h2>
                </div>
                <div className={'mt-6 grid grid-cols-4 gap-8 px-24'}>
                    {
                        evaluationType.map((ev) => {
                            if (ev.id > 1) {
                                return (
                                    <Card className={'transition duration-100 ease-in-out hover:bg-blue-400 hover:text-white'}>
                                    <CardContent className={' text-center px-2'} key={ev.id}>

                                        <h4 className={'text-sm font-semibold mb-4'}>{ev.type}</h4>
                                        {
                                            ev.value >= 13 && ev.value <= 16 ?
                                                <div className={'bg-green-500 rounded mx-6'}>{ev.value}</div> :
                                                ev.value >= 8 && ev.value <= 12 ?
                                                    <span className={'bg-yellow-300 rounded mx-6'}>{ev.value}</span> :
                                                    ev.value >= 4 && ev.value <= 7 ?
                                                        <span className={'bg-red-500 rounded mx-6'}>{ev.value}</span> :
                                                        <span>{ev.value}</span>
                                        }
                                    </CardContent>
                            </Card>
                            )
                            }
                            })

                    }
                    <Card className={'transition duration-100 ease-in-out  hover:bg-blue-400 hover:text-white col-start-2 col-span-2'}>
                        <CardContent className={'text-center px-2'} >
                            <h4 className={'text-2xl font-semibold mb-4'}>RESULTADO TOTAL</h4>
                            <span className={'text-xl'}>{total}</span>
                        </CardContent>
                    </Card>


                </div>

            </div>
            <div className={'flex flex-row justify-between md:content-end md:pl-0  md:pb-0  w-full flex-wrap'}>
                <div className={'hidden md:flex'}>
                    <img src={cloudAlone} alt="" className={'cloud-alone'}/>
                </div>


                <div className={'md:mr-64 md:ml-0 ml-48'}>


                </div>
            </div>

        </div>
    )
}
const mapStateToProps = (state) => {

    return {
        evaluation: state.evaluation
    }
}
const mapDispatchToProps = {
    nextQuestionEvaluation
}
export default connect(mapStateToProps, mapDispatchToProps)(Result);
