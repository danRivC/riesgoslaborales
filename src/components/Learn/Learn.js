import React from "react";
import {useState} from 'react';
import cloud from "../../assets/images/cloud.svg";
import cloudAlone from "../../assets/images/C.svg";
import Button from "@material-ui/core/Button";
import { blue, purple } from '@material-ui/core/colors';
import { createMuiTheme, withStyles, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import {Link, Redirect} from '@reach/router'
import {connect} from "react-redux";
import {nextTopic, backTopic} from "../../actions";
import './Learn.scss';
import {FaRegArrowAltCircleLeft, FaHome} from 'react-icons/fa'

const ColorButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(blue[500]),
        backgroundColor: blue[500],
        '&:hover': {
            backgroundColor: blue[700],
        },
    },
}))(Button);

const Learn = ({learn, nextTopic, backTopic}) => {

    const {content} = learn;
    const [actualContent] = content.filter(topic => (topic.id === learn.actualTopic))
    const backQuestionButton = () => (
        <button onClick={backTopic}>
            <FaRegArrowAltCircleLeft className={'text-gray-600 text-5xl'}/>
        </button>
    )
    const backHomeButton = () => (
        <Link to={'/'}>
            <FaHome className={'text-gray-600 text-5xl'}/>
        </Link>
    )
    const showResult=()=>{
        return(
            console.log('En construccion')
        )
    }


    return (
        <div className={'bg-gray-200 grid grid-cols-1 md:pt-10 md:pl-0 h-full'}>
            <div className={'grid grid-cols-1 gap-64 z-0 absolute w-full'}>
                <div className={'hidden md:flex justify-end'}>
                    <img src={cloud} className={'ml-64'} alt=""/>
                </div>
            </div>
            <div className={'grid grid-cols-1 z-10 '}>
                <div className={'transition duration-500 ease-in-out md:ml-20 md:pt-12 pl-10 pt-10 '}>
                    {actualContent.id > 1 ? backQuestionButton() : backHomeButton()}
                </div>
                <div className={'md:mt-20 text-center'}>
                    <h2 className={'text-4xl md:text-6xl text-gray-500 text-bold'}>{actualContent.tittle}</h2>
                </div>
                <div className={'mt-6 px-4 text-center'}>
                    <p className={'text-1xl md:text-3xl text-gray-500'}>
                        {actualContent.content}
                    </p>
                </div>

            </div>
            <div className={'flex flex-row justify-between md:content-end md:pl-0  md:pb-0  w-full flex-wrap'}>
                <div className={'hidden md:flex'}>
                    <img src={cloudAlone} alt="" className={'cloud-alone'}/>
                </div>


                <div className={'md:mr-64 md:ml-0 ml-48'}>
                    {
                        content.length  === actualContent.id ?
                            <ColorButton variant="contained" color="primary" onClick={showResult}>
                                <span className={'md:text-xl'}>
                                    Finalizar
                                </span>

                            </ColorButton>
                            :
                            <ColorButton variant="contained" color="secondary" onClick={nextTopic}>
                                <span className={'md:text-xl'}>Continuar</span>
                            </ColorButton>
                    }

                </div>
            </div>

        </div>
    )
}
const mapStateToProps = (state) => {

    return {
        learn: state.learn
    }
}
const mapDispatchToProps = {
    nextTopic,
    backTopic
}
export default connect(mapStateToProps, mapDispatchToProps)(Learn);
