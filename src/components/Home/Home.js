import React, {useState} from 'react'
import {Grid} from "@material-ui/core";
import './Home.scss'
import {Link} from "@reach/router";
import {GiSpellBook} from 'react-icons/gi';
import {FaRegArrowAltCircleRight} from 'react-icons/fa';

import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {SiGooglesearchconsole} from "react-icons/si";

const Home = () => {
    const [showLearn, useShowLearn] = useState(false)
    const [showTest, useShowTest] = useState(false)


    return (
        <div className={'home-background grid grid-cols-3 grid-rows-3  h-full'}>

            <div
                className={!showLearn ? "col-start-1  row-start-3 px-10" : "col-start-1 place-items-stretch row-span-3 px-10 bg-gray-900 bg-opacity-50 "}
                onMouseEnter={() => useShowLearn(true)}
                onMouseLeave={() => useShowLearn(false)}
            >
                <Link
                    to="learn"
                >
                    {
                        showLearn ?
                            <ReactCSSTransitionGroup transitionName={'showTest'} transitionAppear={true}
                                                     transitionAppearTimeout={50} transitionEnter={false}
                                                     transitionLeave={false}>
                                <div
                                    className={'transition duration-500 ease-in-out pt-10 text-white place-self-stretch text-center'}>

                                    <GiSpellBook className={'text-white text-center text-2xl md:text-6xl mx-auto'}/>

                                    <h3 className={'text-xl  md:text-3xl py-4'}>
                                        Aprende los conceptos fundamentales acerca de la seguridad ocupacional
                                    </h3>
                                    <p className={'text-xl py-8'}>
                                        En esta sección aprenderás los conceptos más importantes de la seguridad
                                        ocupacional
                                        y estarás apto para entenderlos e identificarlos en tu día a día <br/>
                                        Pon a prueba lo aprendido en un test didactico en donde se evaluará lo
                                        aprendido.
                                        <br/>


                                    </p>
                                    <FaRegArrowAltCircleRight title={'Empezar'}
                                                              className={'text-white text-center text-2xl md:text-6xl mx-auto'}/>
                                </div>
                            </ReactCSSTransitionGroup>

                            :
                            <div className={'text-center'}>
                                <h3 className={"text-white text-xl  md:text-3xl "}>Aprende sobre seguridad
                                    ocupacional</h3>
                            </div>
                    }
                </Link>


            </div>
            <div className={'col-start-2 row-start-2 text-center'}>
                <h1 className={"text-gray-200 text-xl md:text-5xl "}>Seguridad Ocupacional</h1>
            </div>
            <div
                className={!showTest ? "col-start-3  row-start-3 px-10" : "col-start-3 row-start-1 text-center row-span-3 px-10 bg-gray-900 bg-opacity-50"}
                onMouseEnter={() => useShowTest(true)}
                onMouseLeave={() => useShowTest(false)}>
                <Link
                    to="evaluation"
                >
                    {
                        showTest ?
                            <ReactCSSTransitionGroup transitionName={'showTest'} transitionAppear={true}
                                                     transitionAppearTimeout={50} transitionEnter={false}
                                                     transitionLeave={false}>

                                <div
                                    className={'transition duration-500 ease-in-out pt-10 text-white place-self-stretch text-center'}>
                                    <SiGooglesearchconsole
                                        className={'text-white text-center text-2xl md:text-6xl mx-auto'}/>

                                    <h3 className={'text-xl  md:text-3xl py-4'}>
                                        Evalua el riesgo psicosocial en tu trabajo
                                    </h3>
                                    <p className={'text-xl py-8'}>
                                        En esta sección aprenderas a evaluar los factores de riesgo psicosocial que
                                        pueden afectar a tu salud, generando acciones para prevenir o disminuir el
                                        riesgo psicosocial.
                                        <br/>
                                    </p>
                                    <FaRegArrowAltCircleRight title={'Empezar'}
                                                              className={'text-white text-center text-2xl md:text-6xl mx-auto'}/>
                                </div>
                            </ReactCSSTransitionGroup>

                            :
                            <div className={"col-start-3 row-start-3 text-center"}>
                                <h3 className={"text-white text-xl md:text-3xl "}>Evalua el riesgo psicosocial en tu trabajo</h3>
                            </div>

                    }
                </Link>
            </div>
        </div>


    )
}
export default Home;
