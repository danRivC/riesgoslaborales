export default function content() {
    return (
        [
            {
                tittle: 'Riesgo',
                content: 'Es el resultado de la combinación de Frecuencia y Probabilidad, es decir es una medida de la magnitud de los daños frente a una situación peligrosa. (Glosario riesgos laborales, 2016).',
                type: 'Concepto',
                id: 1,
                assets: []
            },
            {
                tittle: 'Peligro',
                content: 'Situación capaz de producir daños en términos de lesiones a la propiedad y al ambiente o una combinación de ellos (Glosario riesgos laborales, 2016).',
                type: 'Concepto',
                id: 2,
                assets: []
            },
            {
                tittle: 'Riesgo laboral',
                content: 'Son aquellos acontecimientos que pueden provocar que un trabajador sufra un determinado daño derivado del trabajo durante el ejercicio de su función.¨ (Pérez, 2009)',
                type: 'Concepto',
                id: 3,
                assets: []
            },
            {
                tittle: 'Accidentes al ir o volver al trabajo (IN ITINERE)',
                content: ' En ocasiones los docentes deben realizar grandes desplazamientos diarios al vivir en localidades alejadas de donde se encuentran sus puestos de trabajo. (Pérez, 2009)',
                type: 'Concepto',
                id: 4,
                assets: []
            },
            {
                tittle: 'Análisis de riesgos laborales',
                content: 'Es un procedimiento que lleva a integrar los principios y prácticas de salud y seguridad, cada paso básico del trabajo se examina para identificar riesgos potenciales y determinar la forma más segura de hacer el trabajo, se basa en la idea de que la seguridad es una parte integral de todo trabajo. (Centro Canadiense de salud, 1998).',
                type: 'Concepto',
                id: 5,
                assets: []
            },
            {
                tittle: 'Factores o condiciones de seguridad',
                content: 'Condiciones materiales que influyen sobre la accidentalidad: pasillos y superficies de tránsito, vehículos de transporte, máquinas, herramientas, espacios de trabajo, instalaciones eléctricas (Castro, 2012).',
                type: 'Concepto',
                id: 6,
                assets: []
            },
            {
                tittle: 'Factores de origen físico, químico y biológico',
                content: 'Se incluyen los denominados contaminantes o agentes físicos (ruido, vibraciones, iluminación, radiaciones ionizantes, rayos X, rayos gamma, entre otros y no ionizantes; ultravioletas, infrarrojas, microondas (Castro, 2012).',
                type: 'Concepto',
                id: 7,
                assets: []
            },
            {
                tittle: 'Factores derivados de las características del trabajo',
                content: 'Incluye esfuerzos, posturas de trabajo, niveles de atención, asociadas a cada tipo de actividad y determinantes de la carga de trabajo, tanto física como mental, pudiera ocasionar la fatiga. (Castro, 2012).',
                type: 'Concepto',
                id: 8,
                assets: []
            },
            {
                tittle: 'Factores derivados de la organización del trabajo',
                content: 'El área de la psicosociología se encarga de los factores de la organización del trabajo (tareas que lo integran y asignación a los trabajadores, horarios, velocidad de ejecución, jerarquías, entre otras) (Castro, 2012).',
                type: 'Concepto',
                id: 9,
                assets: []
            },
            {
                tittle: 'Riesgos eléctricos',
                content: 'Se ocasionan por la manipulación de diferentes aparatos que se utilizan como soporte para impartir las clases como por ejemplo, proyectores, ordenadores portátiles etc, se ocasionan estos riesgos al conectar aparatos a enchufes que no están en buenas condiciones de uso o que han sido manipulados o rotos por los estudiantes (Pérez, 2009).',
                type: 'Factores o condiciones de seguridad',
                id: 10,
                assets: []
            },
            {
                tittle: 'Caídas al mismo nivel',
                content: 'Se producen por el mal estado del suelo, suelos resbaladizos, pasillos ocupados por mochilas y por la existencia de obstáculos, incorrecta disposición del mobiliario y falta de limpieza en las aulas (Pérez, 2009).',
                type: 'Factores o condiciones de seguridad',
                id: 11,
                assets: []
            },
            {
                tittle: 'Incendios',
                content: ' Los incendios pueden producirse por:',
                type: 'Factores o condiciones de seguridad',
                id: 12,
                assets: [
                    {
                        content: 'Utensilios de fumadores: provocadas por cigarrillos, cerillas encendidas, mecheros',
                        id: 1
                    },
                    {
                        content: 'Actos vandálicos: Los incendios provocados.  (Instituto Canario de Seguridad laboral, 1998).',
                        id: 2
                    },
                    {
                        content: 'Los riesgos a los que los docentes están expuestos en el caso de que suceda un incendio:',
                        id: 3
                    },
                    {
                        content: 'Insuficiencia de oxigeno',
                        id: 4
                    },
                    {
                        content: 'Calor',
                        id: 5
                    },
                    {
                        content: 'Quemaduras',
                        id: 6
                    },
                    {
                        content: 'Pánico',
                        id: 7
                    },
                    {
                        content: 'Humos y gases calientes (Instituto Canario de Seguridad laboral, 1998).',
                        id: 8
                    }
                ]
            },
            {
                tittle: 'Ruido',
                content: 'En el interior del aula, el silencio por lo general no es una condición habitual, por lo cual el docente debe ir elevando cada vez más el tono de voz, a esta situación habría que sumar los ruidos que vienen del exterior, haciéndose esta situación intolerable.',
                type: 'Factores de origen físico, biológico y químico',
                id: 13,
                assets: []

            },
            {
                tittle: 'Condiciones termo higrométricas.',
                content: 'Las condiciones termohigrométricas son las condiciones de temperatura, humedad, ventilación y presión atmosférica del ambiente que mal reguladas, pueden dar lugar al riesgo térmico, esto es, cuando la temperatura interna del cuerpo aumenta o disminuye 1 grado centígrado (+/- 10) respecto a la temperatura media del cuerpo (37ºC). En este sentido, el estrés térmico es un estado de malestar físico provocado por una exposición excesiva al frío o al calor (Instituto Canario, s.f.).',
                type: 'Factores de origen físico, biológico y químico',
                id: 14,
                assets: []

            },
            {
                tittle: 'Iluminación',
                content: 'Una iluminación no adecuada, así como molestos reflejos sobre las pizarras de las aulas (provocando molestias al alumnado), obligan a trabajar con luz artificial durante la totalidad de la jornada laboral (Pérez, 2009).',
                type: 'Factores de origen físico, biológico y químico',
                id: 15,
                assets: []

            },
            {
                tittle: 'Ventilación',
                content: 'Las principales medidas e indicaciones que contempla la normativa en cuanto a la ventilación en el lugar de trabajo son: La ventilación en el lugar de trabajo puede ser natural o forzada mecánicamente (por ejemplo, mediante ventiladores). (Riesgos de los profesores, 2012). La ventilación se debe tener en cuenta en la evaluación de los agentes físicos que pueden comportar un riesgo en el entorno laboral.',
                type: 'Factores de origen físico, biológico y químico',
                id: 16,
                assets: []

            },
            {
                tittle: 'Virus y bacterias',
                content: 'Riesgo biológico es la exposición a agentes vivos capaces de originar cualquier tipo de infección, alergia o toxicidad. Aunque, en general, el riesgo biológico suele tener menor entidad que otros riesgos laborales (químicos, físicos, psíquicos o ergonómicos), afecta de forma muy especial al colectivo docente constituyendo la causa principal de ausencia al puesto de trabajo, debido a que al estar un gran número de personas encerradas en una clase con un número reducido de metros cuadrados se pueden enfermar con Gripe, gripe A, rubeola, tétanos, hepatitis B, etc. (Matas, 2009).',
                type: 'Factores de origen físico, biológico y químico',
                id: 17,
                assets: []

            },
            {
                tittle: 'Químicos',
                content: 'Las prácticas que se realizan en los laboratorios pueden presentar una serie de riesgos de origen y consecuencias muy variadas: relacionados con las propias instalaciones de los laboratorios, con los productos químicos que se manejan y con las operaciones que con ellos se realizan. (Universidad de la Rioja, s.f.). El trabajo que se realiza en un laboratorio implica un riesgo potencialmente grande, aunque el riesgo real sea pequeño.',
                type: 'Factores de origen físico, biológico y químico',
                id: 18,
                assets: [
                    {
                        content: 'Adopción de vicios en el trabajo',
                        id: 1
                    },
                    {
                        content: 'Resistencia a la aceptación de normas/reglas.',
                        id: 2
                    },
                    {
                        content: 'El no cumplimiento de las normas de seguridad, cuando las hay, procedimientos de trabajo.',
                        id: 3
                    },
                    {
                        content: 'Desconocimiento',
                        id: 4
                    },
                    {
                        content: 'Falta de Formación e Información.',
                        id: 5
                    },
                    {
                        content: 'Exceso de confianza en la tecnología',
                        id: 6
                    }
                ]
            },
            {
                tittle: 'Problemas de voz.',
                content: 'Estas patologías laborales no están reconocidas como enfermedad profesional, en escasas ocasiones se ha llegado a reconocer como accidentes de trabajo. sector. (Calera et al, 2009). Uno de los principales problemas de salud que sufren los profesores es el de su aparato fonador. Los problemas de la voz aparecen relacionados con: el tamaño de las aulas, alto número de alumnos, jornadas con horarios prolongados y el ruido ambiental elevado. Las lesiones que con mayor frecuencia presentan los docentes y las docentes son: nódulos, edemas y pólipos de cuerdas vocales.',
                type: 'Factores derivados de la característica del trabajo',
                id: 19,
                assets: []

            },
            {
                tittle: 'Disfonías',
                content: 'Se refiere a la alteración de la voz., la mayor parte de estas patologías se deben en el ámbito laboral, a un sobreesfuerzo, a un mal uso de la voz. (Calera, 2009), se conoce como afonía y ronquera.',
                type: 'Factores derivados de la característica del trabajo',
                id: 20,
                assets: []

            },
            {
                tittle: 'Problemas musculoesqueléticos.',
                content: 'Los problemas musculoesqueleticos, no solo se originan por realizar trabajos pesados (construcción, minería), se originan por mantener una misma postura (de pie o sentado), esta postura obliga al profesor a desplazarse, flexionar el tronco, girar el cuerpo o permanecer en una misma posición durante un espacio prolongado de tiempo (8 horas), esto provoca fatiga y dolores musculares. Las lesiones que afectan al sistema músculo-esquelético, constituyen una de las tres principales causas de baja por enfermedad entre los docentes. (Calera et al, 2009).',
                type: 'Factores derivados de la característica del trabajo',
                id: 21,
                assets: []

            },
            {
                tittle: 'Trabajo con pantallas de visualización de datos',
                content: 'El trabajo con pantallas de visualización de datos es quizás el ejemplo más característico de cómo una nueva tecnología puede suponer la introducción de unos nuevos riesgos. Dadas las características que conllevan los trabajos con pantallas de visualización, en los diversos campos de actividad del sector de enseñanza (ya sean pedagógicos o administrativos), la lectura de información presentada sobre pantallas de visualización ha tenido un desarrollo destacado que se ha ampliado considerablemente por la generalización del empleo del ordenador (aplicaciones de tratamiento de textos, uso de Internet, etc.). El mayor porcentaje de aplicación lo representa la visualización y el tratamiento de informaciones sobre pantallas, bien en utilización permanente o temporal.',
                type: 'Factores derivados de la característica del trabajo',
                id: 22,
                assets: []
            },
            {
                tittle: 'Estrés',
                content: 'Las diferentes demandas del medio son excesivas o amenazantes para el bienestar del docente, se presentan sentimientos de tensión física o emocional; según estudios, la profesión docente es aquella que soporta mayor nivel de estrés. El profesor sufre estrés cuando siente que la situación le supera, que no la puede controlar y que le va a impedir realizar su trabajo correctamente. El estrés nace de las características propias de las tareas que realiza el profesor, que le exigen entrega, implicación, contacto con los demás.',
                type: 'Factores derivados de la organización del trabajo',
                id: 23,
                assets: []

            },
            {
                tittle: 'Burnout: Síndrome del quemado',
                content: 'El síndrome de “Burnout”, también llamado síndrome de “quemarse por el trabajo”, de estar quemado o de desgaste profesional, se considera como la fase avanzada del estrés profesional, y se produce cuando se desequilibran las expectativas en el ámbito profesional y la realidad del trabajo diario.\n' +
                    'Se define como una respuesta al estrés laboral crónico integrado por actitudes y sentimientos negativos hacia las personas con las que se trabaja y hacia el propio rol profesional, así como por la vivencia de encontrarse emocionalmente agotado. Esta respuesta ocurre con frecuencia en los profesionales de la salud y, en general, en profesionales de organizaciones de servicios que trabajan en contacto directo con personas.\n',
                type: 'Factores derivados de la organización del trabajo',
                id: 24,
                assets: []

            },
            {
                tittle: 'Violencia laboral.',
                content: 'Estas patologías laborales no están reconocidas como enfermedad profesional, en escasas ocasiones se ha llegado a reconocer como accidentes de trabajo. sector. (Calera et al, 2009). Uno de los principales problemas de salud que sufren los profesores es el de su aparato fonador. Los problemas de la voz aparecen relacionados con: el tamaño de las aulas, alto número de alumnos, jornadas con horarios prolongados y el ruido ambiental elevado. Las lesiones que con mayor frecuencia presentan los docentes y las docentes son: nódulos, edemas y pólipos de cuerdas vocales.',
                type: 'Factores derivados de la organización del trabajo',
                id: 25,
                assets: []

            },
            {
                tittle: 'Mobbing',
                content: 'Según Leymann (1984), podemos definir el acoso psicológico o “mobbing” como la situación en la que una persona o grupo de personas ejercen una violencia psicológica extrema (en alguna de las formas tipificadas en su método) sobre otra de forma sistemática (al menos una vez a la semana) y durante un tiempo prolongado (mínimo seis meses) en el lugar de trabajo.',
                id: 26,
                assets: []
            }
        ]

    )
}


