import Home from "./components/Home/Home";

import './assets/styles/_styles.scss';
import './assets/styles/custom_styles.scss';
import { Router, Link } from "@reach/router"
import Learn from "./components/Learn/Learn";
import Test from "./components/Test/Test";
import Result from "./components/Result/Result";

function RouterComponent({ children }) {
    // Shorthand of <React.Fragment>{children}</React.Fragment>
    return <>{children}</>;
}

function App() {
  return (

      <Router primary={false} component={RouterComponent}>
        <Home path={'/'}/>
        <Learn path={'learn'}/>
        <Test path={'evaluation'}/>
        <Result path={'evaluation/results'}/>
      </Router>

  );
}

export default App;
