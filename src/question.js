export default function questions() {
    return (
        [
            {
                id: 1,
                tittle: 'Área de trabajo:',
                type: 'INFORMACION BASICA',
                options: [
                    {id: 1, tittle: 'Administrativa', value: 1},
                    {id: 2, tittle: 'Operativa', value: 2}
                ],
            },
            {
                id: 2,
                tittle: 'Nivel más alto de instrucción (Marque una sola opción) :',
                type: 'INFORMACION BASICA',
                options: [
                    {id: 1, tittle: 'Ninguno', value: 1},
                    {id: 2, tittle: 'Educación básica', value: 2},
                    {id: 3, tittle: 'Educación media', value: 3},
                    {id: 4, tittle: 'Bachillerato', value: 4},
                    {id: 5, tittle: 'Técnico / Tecnológico', value: 5},
                    {id: 6, tittle: 'Tercer nivel', value: 6},
                    {id: 7, tittle: 'Cuarto nivel', value: 7},
                    {id: 8, tittle: 'Otro', value: 8}
                ],
            },
            {
                id: 3,
                tittle: 'Antigüedad, años de experiencia dentro de la empresa o institución:',
                type: 'INFORMACION BASICA',
                options: [
                    {id: 1, tittle: '0-2 años', value: 1},
                    {id: 2, tittle: '3-10 años', value: 2},
                    {id: 3, tittle: '11-20 años', value: 3},
                ],
            },
            {
                id: 4,
                tittle: 'Edad del trabajador o servidor:',
                type: 'INFORMACION BASICA',
                options: [
                    {id: 1, tittle: '16-24 años', value: 1},
                    {id: 2, tittle: '25-34 años', value: 2},
                    {id: 3, tittle: '35-43 años', value: 3},
                    {id: 4, tittle: '44-52 años', value: 4},
                    {id: 5, tittle: 'Igual o superior a 53 años', value: 5}
                ],
            },
            {
                id: 5,
                tittle: 'Auto-identificación étnica:',
                type: 'INFORMACION BASICA',
                options: [
                    {id: 1, tittle: 'Indígena', value: 1},
                    {id: 2, tittle: 'Mestizo/a:', value: 2},
                    {id: 3, tittle: 'Montubio/a:', value: 3},
                    {id: 4, tittle: 'Afro-ecuatoriano:', value: 4},
                    {id: 5, tittle: 'Blanco/a:', value: 5},
                    {id: 6, tittle: 'Otro:', value: 6}
                ],
                total: 0
            },
            {
                id: 6,
                tittle: 'Considero que son aceptables las solicitudes y requerimientos que me piden otras personas (compañeros de trabajo, usuarios, clientes).',
                type: 'CARGA Y RITMO DE TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],

            },
            {
                id: 7,
                tittle: 'Decido el ritmo de trabajo en mis actividades.',
                type: 'CARGA Y RITMO DE TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 8,
                tittle: 'Las actividades y/o responsabilidades que me fueron asignadas no me causan estrés.',
                type: 'CARGA Y RITMO DE TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],

            },
            {
                id: 9,
                tittle: 'Tengo suficiente tiempo para realizar todas las actividades que me han sido encomendadas dentro de mi jornada laboral.',
                type: 'CARGA Y RITMO DE TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],

            },
            {
                id: 10,
                tittle: 'Considero que tengo los suficientes conocimientos, habilidades y destrezas para desarrollar el trabajo para el cual fuí contratado',
                type: 'DESARROLLO DE COMPETENCIAS',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 11,
                tittle: 'En mi trabajo aprendo y adquiero nuevos conocimientos, habilidades y destrezas de mis compañeros de trabajo',
                type: 'DESARROLLO DE COMPETENCIAS',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 12,
                tittle: 'En mi trabajo se cuenta con un plan de carrera, capacitación y/o entrenamiento para el desarrollo de mis conocimientos, habilidades y destrezas',
                type: 'DESARROLLO DE COMPETENCIAS',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 13,
                tittle: 'En mi trabajo se evalúa objetiva y periódicamente las actividades que realizo',
                type: 'DESARROLLO DE COMPETENCIAS',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 14,
                tittle: 'En mi trabajo se reconoce y se da crédito a la persona que realiza un buen trabajo o logran sus objetivos.',
                type: 'LIDERAZGO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 15,
                tittle: 'Mi jefe inmediato esta dispuesto a escuchar propuestas de cambio e iniciativas de trabajo',
                type: 'LIDERAZGO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 16,
                tittle: 'Mi jefe inmediato establece metas, plazos claros y factibles para el cumplimiento de mis funciones o actividades',
                type: 'LIDERAZGO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 17,
                tittle: 'Mi jefe inmediato interviene, brinda apoyo, soporte y se preocupa cuando tengo demasiado trabajo que realizar',
                type: 'LIDERAZGO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 18,
                tittle: 'Mi jefe inmediato me brinda suficientes lineamientos y retroalimentación para el desempeño de mi trabajo',
                type: 'LIDERAZGO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 19,
                tittle: 'Mi jefe inmediato pone en consideración del equipo de trabajo, las decisiones que pueden afectar a todos.',
                type: 'LIDERAZGO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 20,
                tittle: 'En mi trabajo existen espacios de discusión para debatir abiertamente los problemas comunes y diferencias de opinión',
                type: 'MARGEN DE ACCIÓN Y CONTROL',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 21,
                tittle: 'Me es permitido realizar el trabajo con colaboración de mis compañeros de trabajo y/u otras áreas',
                type: 'MARGEN DE ACCIÓN Y CONTROL',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 22,
                tittle: 'Mi opinión es tomada en cuenta con respecto a fechas límites en el cumplimiento de mis actividades o cuando exista cambio en mis funciones',
                type: 'MARGEN DE ACCIÓN Y CONTROL',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 23,
                tittle: 'Se me permite aportar con ideas para mejorar las actividades y la organización del trabajo',
                type: 'MARGEN DE ACCIÓN Y CONTROL',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 24,
                tittle: 'Considero que las formas de comunicación en mi trabajo son adecuados, accesibles y de fácil comprensión',
                type: 'ORGANIZACIÓN DEL TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 25,
                tittle: 'En mi trabajo se informa regularmente de la gestión y logros de la empresa o institución a todos los trabajadores y servidores',
                type: 'ORGANIZACIÓN DEL TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 26,
                tittle: 'En mi trabajo se respeta y se toma en consideración las limitaciones de las personas con discapacidad para  la asignación de roles y tareas',
                type: 'ORGANIZACIÓN DEL TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 27,
                tittle: 'En mi trabajo tenemos reuniones suficientes y significantes para el cumplimiento de los objetivos',
                type: 'ORGANIZACIÓN DEL TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 28,
                tittle: 'Las metas y objetivos en mi trabajo son claros y alcanzables',
                type: 'ORGANIZACIÓN DEL TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 29,
                tittle: 'Siempre dispongo de tareas y actividades a realizar en mi jornada y lugar de trabajo',
                type: 'ORGANIZACIÓN DEL TRABAJO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 30,
                tittle: 'Después del trabajo tengo la suficiente energía como para realizar otras actividades',
                type: 'RECUPERACIÓN',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 31,
                tittle: 'En mi trabajo se me permite realizar pausas de periodo corto para renovar y recuperar la energía.',
                type: 'RECUPERACIÓN',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 32,
                tittle: 'En mi trabajo tengo tiempo para dedicarme a reflexionar sobre mi desempeño en el trabajo',
                type: 'RECUPERACIÓN',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 33,
                tittle: 'Tengo un horario y jornada de trabajo que se ajusta a mis expectativas y exigencias laborales',
                type: 'RECUPERACIÓN',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 34,
                tittle: 'Todos los días siento que he descansado lo suficiente y que tengo la energía para iniciar mi trabajo',
                type: 'RECUPERACIÓN',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 35,
                tittle: 'El trabajo está organizado de tal manera que  fomenta la colaboración de equipo y el diálogo con otras personas',
                type: 'SOPORTE Y APOYO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 36,
                tittle: 'El trabajo está organizado de tal manera que  fomenta la colaboración de equipo y el diálogo con otras personas',
                type: 'SOPORTE Y APOYO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 37,
                tittle: 'En mi trabajo percibo un sentimiento de compañerismo y bienestar con mis colegas',
                type: 'SOPORTE Y APOYO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 38,
                tittle: 'En mi trabajo se brinda el apoyo necesario a los trabajadores sustitutos o trabajadores con algún grado de discapacidad y enfermedad',
                type: 'SOPORTE Y APOYO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 39,
                tittle: 'En mi trabajo se me brinda ayuda técnica y administrativa cuando lo requiero',
                type: 'SOPORTE Y APOYO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 40,
                tittle: 'En mi trabajo tengo acceso a la atención de un médico, psicólogo, trabajadora social, consejero, etc. en situaciones de crisis y/o rehabilitación',
                type: 'SOPORTE Y APOYO',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 41,
                tittle: 'En mi trabajo tratan por igual a todos, indistintamente la edad que tengan',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 42,
                tittle: 'Las directrices y metas que me autoimpongo, las cumplo dentro de mi jornada y horario de trabajo',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 43,
                tittle: 'En mi trabajo existe un buen ambiente laboral',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 44,
                tittle: 'Tengo un trabajo donde los hombres y mujeres tienen las mismas oportunidades',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 45,
                tittle: 'En mi trabajo me siento aceptado y valorado',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 46,
                tittle: 'Los espacios y ambientes físicos en mi trabajo brindan las facilidades para el acceso de las personas con discapacidad',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 47,
                tittle: 'Considero que mi trabajo esta libre de amenazas, humillaciones, ridiculizaciones, burlas, calumnias o difamaciones reiteradas con el fin de causarme daño.',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 48,
                tittle: 'Me siento estable a pesar de cambios que se presentan en mi trabajo.',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 49,
                tittle: 'En mi trabajo estoy libre de conductas sexuales que afecten mi integridad física, psicológica y moral',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 50,
                tittle: 'Considero que el trabajo que realizo no me causa efectos negativos a mi salud física y mental',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 51,
                tittle: 'Me resulta fácil relajarme cuando no estoy trabajando',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 52,
                tittle: 'Siento que mis problemas familiares o personales no influyen en el desempeño de las actividades en el trabajo',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 53,
                tittle: 'Las instalaciones, ambientes, equipos, maquinaria y herramientas que utilizo para realizar el trabajo son las adecuadas para no sufrir accidentes de trabajo y enfermedades profesionales',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 54,
                tittle: 'Mi trabajo esta libre de acoso sexual',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 55,
                tittle: 'En mi trabajo se me permite solucionar mis problemas familiares y personales',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 56,
                tittle: 'Considero que el trabajo que realizo no me causa efectos negativos a mi salud física y mental',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 57,
                tittle: 'Me resulta fácil relajarme cuando no estoy trabajando',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 58,
                tittle: 'Siento que mis problemas familiares o personales no influyen en el desempeño de las actividades en el trabajo',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 59,
                tittle: 'Las instalaciones, ambientes, equipos, maquinaria y herramientas que utilizo para realizar el trabajo son las adecuadas para no sufrir accidentes de trabajo y enfermedades profesionales',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 60,
                tittle: 'Mi trabajo esta libre de acoso sexual',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 61,
                tittle: 'En mi trabajo se me permite solucionar mis problemas familiares y personales',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 62,
                tittle: 'Tengo un trabajo libre de conflictos estresantes, rumores maliciosos o calumniosos sobre mi persona.',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 63,
                tittle: 'Tengo un equilibrio y separo bien el trabajo de mi vida personal.',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 64,
                tittle: 'Estoy orgulloso de trabajar en mi empresa o institución',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 65,
                tittle: 'En mi trabajo se respeta mi ideología, opinión política, religiosa, nacionalidad y orientación sexual.',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 66,
                tittle: 'Mi trabajo y los aportes que realizo son valorados y me generan motivación.',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 67,
                tittle: 'Me siento libre de culpa cuando no estoy trabajando en algo',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 68,
                tittle: 'En mi trabajo no existen espacios de uso exclusivo de un grupo determinado de personas ligados a un privilegio, por ejemplo, cafetería exclusiva, baños exclusivos, etc., mismo que causa malestar y perjudica mi ambiente laboral',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 69,
                tittle: 'Puedo dejar de pensar en el trabajo durante mi tiempo libre (pasatiempos, actividades de recreación, otros) ',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },
            {
                id: 70,
                tittle: 'Considero que me encuentro física y mentalmente saludable',
                type: 'OTROS PUNTOS IMPORTANTES',
                options: [
                    {id: 1, tittle: 'Completamente de Acuerdo', value: 4},
                    {id: 2, tittle: 'Parcialmente de Acuerdo', value: 3},
                    {id: 3, tittle: 'Poco de Acuerdo', value: 2},
                    {id: 4, tittle: 'En desacuerdo:', value: 1},
                ],
            },


        ]
    )
}
