const tailwindcss = require('tailwindcss');
module.exports = {
    purge: [
        'src/**/*.js',
        'src/**/*.jsx',
        'src/**/*.ts',
        'src/**/*.tsx',
        'public/**/*.html',
    ],
    plugins: [
        tailwindcss('./tailwind.js'),
        require('autoprefixer')
    ],
};
